mod api;

fn main() {
    let steam = api::steam();
    let steam = match steam {
        Ok(mut s) => {
            s.response.games.sort_by_key(|k| &k.name); //Sorts alphabetically
            s.response
        }
        Err(e) => {
            println!("error fetching API: {:?}", e);
            panic!("fatal error: <{}>", e)
        },
    };
    for game in steam.games {
        println!("[Steam] {}", game.name);
    }
}