use reqwest;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Games {
  pub appid: i64,
  pub name: String,
  pub playtime_forever: i64,
  pub img_icon_url: String,
  pub img_logo_url: String,
  pub has_community_visible_stats: Option<bool>,
  pub playtime_2weeks: Option<i64>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Response {
  pub game_count: i64,
  pub games: Vec<Games>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RootInterface {
  pub response: Response,
}

pub fn steam() -> Result<(RootInterface), Box<std::error::Error>> {
    let resp: RootInterface = reqwest::get(<API KEY GOES HERE>)?
        .json()?;
    Ok(resp)
}